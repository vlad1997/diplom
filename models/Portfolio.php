<?php


class Portfolio
{
    public static function getPortfolioList()
    {
        $db = Db::getConnection();
        $result = $db->query('SELECT id, name, status FROM portfolio ORDER BY id ASC');
        $portfolioList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $portfolioList[$i]['id'] = $row['id'];
            $portfolioist[$i]['name'] = $row['name'];
            $portfolioist[$i]['status'] = $row['status'];
            $i++;
        }
        return $productsList;
    }
    public static function getImage($id)
    {
        $noImage = 'no-image.jpg';
        $path = '/upload/images/portfolio/';
        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            return $pathToProductImage;
        }
        return $path . $noImage;
    }
}