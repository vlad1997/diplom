<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Granit</title>
        <link href="/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/template/css/price-range.css" rel="stylesheet">
        <link href="/template/css/animate.css" rel="stylesheet">
        <link href="/template/css/main.css" rel="stylesheet">
        <link href="/template/css/responsive.css" rel="stylesheet">
        <link href="/template/css/k2.fonts.css?v2.7.1" rel="stylesheet" type="text/css" />
        <link href="/template/css/k2.css?v2.7.1" rel="stylesheet" type="text/css" />
        <link href="/template/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/rokbox.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/menu5-0409cb3d523d8a99e25d108054293189.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/grid-responsive.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/grid-responsive.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/master10-b303c1e5744d1b4f618d61af7361442b.css" rel="stylesheet" />
        <link href="/template/css/mediaqueries.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/settings.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/captions.css" rel="stylesheet" type="text/css" />
        <link href="/template/css/template.css?ba5e6062320daa0e79e5fb6fe5e22c38" rel="stylesheet" type="text/css" />
        <link href="/template/css/chosen.css?ba5e6062320daa0e79e5fb6fe5e22c38" rel="stylesheet" type="text/css" />
        <link href="/template/css/finder.css?ba5e6062320daa0e79e5fb6fe5e22c38" rel="stylesheet" type="text/css" />
        <style type="text/css">
            a, ul.menu li .separator {color: #67292b;}  .button, .readon, .readmore, button.validate, #member-profile a, #member-registration a, .formelm-buttons button, .btn-primary, .style3 .gkTabsWrap.vertical ol li.active, .style3 .gkTabsWrap.horizontal ol li.active, .style3 .gkTabsWrap.vertical ol li.active:hover, .style3 .gkTabsWrap.horizontal ol li.active:hover {border-color: #1e0c0d;}  .gf-menu .dropdown, #rt-footer, .style1 .gkTabsWrap.vertical ol, .ctabutton {border-color: #67292b;}  #rt-login #btl .btl-panel > span#btl-panel-login:hover, #rt-login #btl .btl-panel > span#btl-panel-registration:hover, #rt-login #btl .btl-panel > span#btl-panel-profile:hover, #simplemodal-container a.modalCloseImg:hover, div.k2TagCloudBlock a:hover, .sprocket-mosaic-order ul li:hover, .sprocket-mosaic-loadmore:hover, .title-line span, #Kunena .button,#Kunena .kbutton, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-up, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-up-hovering, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-ie6-up, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-ie6-up-hovering, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-up, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-up-hovering, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-ie6-up, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-ie6-up-hovering, div.ecwid-productBrowser-cart-checkoutButton-up, div.ecwid-productBrowser-cart-checkoutButton-up-hovering, div.ecwid-productBrowser-cart-checkoutButton-ie6-up, div.ecwid-productBrowser-cart-checkoutButton-ie6-up-hovering {background-color: #67292b !important;}  .btl-buttonsubmit input.btl-buttonsubmit,.btl-buttonsubmit button.btl-buttonsubmit, .tp-caption.insightfx_color, div.itemCategory a:hover, div.itemTagsBlock ul.itemTags li a:hover, div.catItemCategory a:hover, div.catItemTagsBlock ul.catItemTags li a:hover, div.catItemReadMore a, div.catItemCommentsLink a, div.tagItemCategory a:hover, div.tagItemReadMore a, div.userItemCategory a:hover, div.userItemTagsBlock ul.userItemTags li a:hover, div.userItemReadMore a, div.userItemCommentsLink a, div.genericItemCategory a:hover, div.genericItemReadMore a, .gkIsButtons, .style1 .gkTabsWrap.vertical ol li:hover, .style1 .gkTabsWrap.vertical ol li.active, .style1 .gkTabsWrap.horizontal ol li:hover, .style1 .gkTabsWrap.horizontal ol li.active, .style3 .gkTabsWrap.vertical ol li.active, .style3 .gkTabsWrap.horizontal ol li.active, .style3 .gkTabsWrap.vertical ol li.active:hover, .style3 .gkTabsWrap.horizontal ol li.active:hover, h4.sprocket-lists-title:hover, .features div:hover p.icon, #featuredprojects ul li:hover, .social li:hover, #Kunena .kpagination span, .ecwid-ProductBrowser-auth-anonim a.gwt-Anchor:hover, html#ecwid_html body#ecwid_body button.gwt-Button, html#ecwid_html body#ecwid_body #wrapper button.gwt-Button, html#ecwid_html body#ecwid_body div.ecwid-productBrowser-subcategories-categoryName:hover, .ecwid-SearchPanel .ecwid-SearchPanel-button {background:#67292b !important;}  .btl-buttonsubmit input.btl-buttonsubmit:hover, .btl-buttonsubmit button.btl-buttonsubmit:hover, div.catItemReadMore a:hover, div.catItemCommentsLink a:hover, div.userItemReadMore a:hover, div.userItemCommentsLink a:hover, div.genericItemReadMore a:hover {background:#4a1d1f !important; }  #bt_ul li a:hover, .jfx_action a:hover, .features div:hover h3, .features div:hover h3 a, a.readon1:hover, #featuredprojects ul li:hover h4, span.itemAuthor:hover a, span.itemAuthor:hover .icon-user, div.itemToolbar ul li a.itemPrintLink:hover, div.itemToolbar ul li a.itemEmailLink:hover, div.itemToolbar ul li a.itemCommentsLink:hover, div.itemAuthorLatest ul li a:hover, a.itemRelTitle:hover, div.catItemHeader h3.catItemTitle a:hover, div.catItemHeader span.catItemAuthor:hover a, div.catItemHeader span.catItemAuthor:hover i, div.tagItemHeader h2.tagItemTitle a:hover, div.userItemHeader h3.userItemTitle a:hover, div.genericItemHeader h2.genericItemTitle a:hover, .sprocket-features-desc a.readon, .gf-menu .dropdown ul li.active > .item, .nspArt h4.nspHeader a:hover, a.nspComments:hover, #rt-footer ul li a:hover, #rt-copyright a:hover, #rt-breadcrumbs a:hover, div.k2ItemsBlock ul li a.moduleItemTitle:hover, div.k2CategoriesListBlock ul li a:hover, div.k2LatestCommentsBlock ul li a:hover, div.k2LatestCommentsBlock ul li a:focus, .sprocket-readmore:hover, .sprocket-mosaic-item .sprocket-mosaic-title a:hover, .blog h2 a:hover, ul.menu li a:hover, form#login-form ul li a:hover, .item-page h2 a:hover, .items-more li a:hover, .component-content .login + div ul li a:hover, .style2 .gkTabsWrap.vertical ol li.active, .style2 .gkTabsWrap.horizontal ol li.active, .style4 .gkTabsWrap.vertical ol li.active, .style4 .gkTabsWrap.horizontal ol li.active, .style4 .gkTabsWrap.vertical ol li:hover, .style4 .gkTabsWrap.horizontal ol li:hover, .sprocket-lists-item a.readon, h4.sprocket-strips-title a, #Kunena #ktab a:hover span, #Kunena #ktab #current a span, #Kunena a:hover, #Kunena div.k_guest b, html#ecwid_html body#ecwid_body div.ecwid-productBrowser-categoryPath a, html#ecwid_html body#ecwid_body div.ecwid-productBrowser-categoryPath a:active, html#ecwid_html body#ecwid_body div.ecwid-productBrowser-categoryPath a:visited, .ecwid-popup-linkBlock a, html#ecwid_html body#ecwid_body div.ecwid-results-topPanel div.ecwid-results-topPanel-viewAsPanel-link, html#ecwid_html body#ecwid_body div.ecwid-productBrowser-productsTable-addToBagLink, html#ecwid_html body#ecwid_body div.ecwid-categoriesMenuBar td.gwt-MenuItem-selected span.ecwid-categories-category, html#ecwid_html body#ecwid_body div.ecwid-categoriesMenuBar td.gwt-MenuItem-current span.ecwid-categories-category, html#ecwid_html body#ecwid_body td.ecwid-categories-vertical-table-cell-selected span.ecwid-categories-category, html#ecwid_html body#ecwid_body table.ecwid-categoriesTabBar table.gwt-TabBarItem-selected span.ecwid-categories-category, html#ecwid_html body#ecwid_body span.ecwid-categories-category:hover, div.ecwid-minicart-link *, a.gwt-Anchor {color:#67292b !important;}  div.k2LatestCommentsBlock ul li span.lcUsername, div.k2LatestCommentsBlock ul li span.lcUsername a, html#ecwid_html body#ecwid_body div.ecwid-productBrowser-price {color:#301314 !important; }  #Kunena #ktab a:hover, #Kunena #ktab #current a {border-top: 5px solid #67292b !important;}  #Kunena .kpagination span {border-color: #67292b !important;}  .button, .readon, .readmore, button.validate, #member-profile a, #member-registration a, .formelm-buttons button, .btn-primary {background:#67292b !important;}  .button:hover, .readon:hover, .readmore:hover, button.validate:hover, #member-profile a:hover, #member-registration a:hover, .formelm-buttons button:hover, .btn-primary:hover, div.tagItemReadMore a:hover, #Kunena .kbutton:hover,#Kunena .kbutton:focus, html#ecwid_html body#ecwid_body button.gwt-Button:hover, html#ecwid_html body#ecwid_body #wrapper button.gwt-Button:hover, .ecwid-SearchPanel .ecwid-SearchPanel-button:hover {background:#4a1d1f !important; }  .button:active, .readon:active, .readmore:active, button.validate:active, #member-profile a:active, #member-registration a:active, .formelm-buttons button:active, .btn-primary:active {background-color: #602628; background: linear-gradient(top, rgba(96,38,40,1) 0%, rgba(132,53,55,1) 100%); background: -moz-linear-gradient(top, rgba(96,38,40,1) 0%, rgba(132,53,55,1) 100%);}
            html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-up:hover, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-up-hovering:hover, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-ie6-up:hover, html#ecwid_html body#ecwid_body div.ecwid-ContinueShoppingButton-ie6-up-hovering:hover, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-up:hover, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-up-hovering:hover, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-ie6-up:hover, html#ecwid_html body#ecwid_body div.ecwid-AddToBagButton-ie6-up-hovering:hover, div.ecwid-productBrowser-cart-checkoutButton-up:hover, div.ecwid-productBrowser-cart-checkoutButton-up-hovering:hover, div.ecwid-productBrowser-cart-checkoutButton-ie6-up:hover, div.ecwid-productBrowser-cart-checkoutButton-ie6-up-hovering:hover {background-color:#4a1d1f !important; }
        </style>
    </head>
    <body>
        <div class="page-wrapper">
            <header id="rt-top-surround">
                <div id="rt-top" >
                    <div class="rt-container">
                        <div class="rt-grid-4 rt-alpha">
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">


                                        <div class="custom"  >
                                            <p><a href="/"><img src="/upload/images/logo_2.png" alt="logo" /></a>
                                            </p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">


                                        <div class="custom"  >

                                            <!-- Код тега ремаркетинга Google -->
                                            <script type="text/javascript">
                                                /* <![CDATA[ */
                                                var google_conversion_id = 963251865;
                                                var google_custom_params = window.google_tag_params;
                                                var google_remarketing_only = true;
                                                /* ]]> */
                                            </script>
                                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                                            </script>
                                            <noscript>
                                                <div style="display:inline;">
                                                    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963251865/?value=0&amp;guid=ON&amp;script=0"/>
                                                </div>
                                            </noscript>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="rt-grid-4">
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">


                                        <div class="custom"  >
                                            <div class="contact-header-wrapper"><p class="contact-header">Київ<span class="item-contact-header"><a href="tel:+380656566565">(065) 656-65-65</a> </span></p>
                                                <p class="contact-header">Львів <span class="item-contact-header"><a href="tel:+380656566565"> (065) 656-65-65</a></span> </p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="rt-grid-4 rt-omega">
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">
                                        <div class="mod-languages">

                                            <ul class="lang-inline">
                                                <li dir="ltr">
                                                </li>
                                                <li class="lang-active" dir="ltr">
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">
                                        <li class="lang-active" dir="ltr">
                                            <a href="/cart/">Корзина</a>
                                        </li>
                                        <form id="mod-finder-searchform308" action="/uk/search" method="get" class="form-search">
                                            <div class="finder">
                                                <label for="mod-finder-searchword308" class="finder">Найдите нужный вам камень</label><br /><input type="text" name="q" id="mod-finder-searchword308" class="search-query input-medium" size="55" value="" placeholder="Пошук..."/><button class="btn btn-primary hasTooltip  finder" type="submit" title="Перейти"><span class="icon-search icon-white"></span>Пошук</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div id="rt-header">
                    <div class="rt-container">
                        <div id="header-shadow"></div>
                        <div class="rt-grid-12 rt-alpha rt-omega">
                            <div class="rt-block menu-block">
                                <div class="gf-menu-device-container"></div>
                                <ul class="gf-menu l1 " >
                                    <li class="item292" >
                                        <a class="item" href="/">Головна</a>
                                    </li>
                                    <li class="item531" >
                                        <a class="item" href="/catalog/">Каталог каменю</a>
                                    </li>
                                    <li class="item287" >
                                        <a class="item" href="/about/">Портфоліо</a>
                                    </li>
                                    <li class="item289" >
                                        <a class="item" href="/contacts/"  >Контакти</a>
                                    </li>
                                </ul>		<div class="clear"></div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </header>