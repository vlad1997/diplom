
<footer id="rt-footer-surround">
    <div id="rt-footer">
        <div class="rt-container">
            <div class="rt-grid-3 rt-alpha">
                <div class="rt-block hidden-phone">
                    <div class="module-surround">
                        <div class="module-title">
                            <h2 class="title"><span>Вироби з каменю</span></h2>
                            <div class="title-line">
                                <span></span>
                            </div>
                        </div>
                        <div class="module-content">


                            <div class="customhidden-phone"  >
                                <ul>
                                    <li><a href="/uk/content">Мармурові стільниці</a></li>
                                    <li><a href="/uk/content">Сходи з мармуру</a></li>
                                    <li><a href="/uk/about-stone/o-kamne">Вироби з мармуру</a></li>
                                    <li><a href="/uk/content">Мармурова плитка</a></li>
                                    <li><a href="/uk/content">Підлога з мармуру</a></li>
                                </ul></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="rt-grid-3">
                <div class="rt-block ">
                    <div class="module-surround">
                        <div class="module-title">
                            <h2 class="title"><span>Офіс в Україні</span></h2>
                            <div class="title-line">
                                <span></span>
                            </div>
                        </div>
                        <div class="module-content">


                            <div class="custom"  >
                                <p>Компанія Granit Україна</p>
                                <div class="aboutus">
                                    <ul>
                                        <li><span class="address">Київ, Жмеринська, 22Б</span></li>
                                        <li><span class="phone"><a href="tel:+380686865475">+38 (066) 656-65-65</a></span></li>
                                        <!--<li><span class="phone"><a href="tel:+380677613017">+38 (067) 761-30-17</a></span></li>-->
                                        <li><span class="email">stone@granit.com.ua</span></li>
                                    </ul>
                                </div></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="rt-grid-3">
                <div class="rt-block ">
                    <div class="module-surround">
                        <div class="module-title">
                            <h2 class="title"><span>Офіс в Італії</span></h2>
                            <div class="title-line">
                                <span></span>
                            </div>
                        </div>
                        <div class="module-content">


                            <div class="custom"  >
                                <p>Granit - head office in Italy</p>
                                <div class="aboutus">
                                    <ul>
                                        <li><span class="address">Verona, via Aquleia, 20</span></li>
                                        <li><span class="phone">+39 (393) 65656565</span></li>
                                        <li><span class="email">italy@granit.com.ua</span></li>
                                    </ul>
                                </div></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="rt-grid-3 rt-omega">
                <div class="rt-block hidden-phone">
                    <div class="module-surround">
                        <div class="module-title">
                            <h2 class="title"><span>Сляби з каменю</span></h2>
                            <div class="title-line">
                                <span></span>
                            </div>
                        </div>
                        <div class="module-content">


                            <div class="customhidden-phone"  >
                                <ul>
                                    <li><a href="/uk/content">Слябы з травертина</a></li>
                                    <li><a href="/uk/content">Мамрумр в слебах</a></li>
                                    <li><a href="/uk/content">Сляби з кварцита</a></li>
                                    <li><a href="/uk/content">Плити з граніту</a></li>
                                    <li><a href="/uk/content">Слеби з оніксу</a></li>
                                </ul></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="rt-copyright">
        <div class="rt-container">
            <div class="rt-grid-6 rt-alpha">
                <div class="clear"></div>
                <div class="rt-block">
                    <span class='copyright'>Granit © 2020 Всі права захищені</span>	</div>

            </div>
            <div class="rt-grid-6 rt-omega">
                <div class="rt-block hidden-phone">
                    <div class="module-surround">
                        <div class="module-content">


                            <div class="customhidden-phone"  >
                                <div class="social clearfix">
                                    <ul>
                                        <li class="facebookli">
                                            <div class="facebook"><a href="#"><span>FB</span></a></div>
                                        </li>
                                        <li class="twitterli">
                                            <div class="twitter"><a href="#"><span>TW</span></a></div>
                                        </li>
                                        <li class="googleli">
                                            <div class="google"><a href="#"><span>G+</span></a></div>
                                        </li>
                                        <li class="rssfeedli">
                                            <div class="instagram"><a href="#"><span>In</span></a></div>
                                        </li>
                                        <li class="vimeoli">
                                            <div class="vimeo"><a href="#"><span>VM</span></a></div>
                                        </li>
                                        <li class="youtubeli">
                                            <div class="youtube"><a href="#"><span>YT</span></a></div>
                                        </li>
                                    </ul>
                                </div></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clear"></div>
        </div>
    </div>
</footer>

<script src="/template/js/jquery.js"></script>
<script src="/template/js/jquery.cycle2.min.js"></script>
<script src="/template/js/jquery.cycle2.carousel.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>

<script>
    $(document).ready(function(){
        $(".add-to-cart").click(function () {
            var id = $(this).attr("data-id");
            $.post("/cart/addAjax/"+id, {}, function (data) {
                $("#cart-count").html(data);
            });
            return false;
        });
    });
</script>

</body>
</html>