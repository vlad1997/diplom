<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>

            <div id="rt-mainbody-surround">
                <div id="rt-breadcrumbs">
                    <div class="rt-container">
                        <div class="rt-grid-12 rt-alpha rt-omega">
                            <div class="rt-block  hidden-phone clearfix breadcustom">
                                <div class="module-surround">
                                    <div class="module-content">

                                        <div class="breadcrumbs hidden-phone clearfix breadcustom">
                                            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb" style="display:inline-block;"><a href="/" class="pathway" itemprop="url"><span  itemprop="title">Granit</span></a></div> <img src="/media/system/images/arrow.png" alt="" /> <div style="display:inline-block;"><span itemprop="title">Контакти</span></div></div>	                	</div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div id="rt-maintop">
                    <div class="rt-container">
                        <div class="rt-grid-6 rt-alpha">
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">
                                        <div class="custom"  >
                                            <h1>Контакти Granit</h1>
                                            <p><span style="font-size: 18pt;">Наші Email:</span></p>
                                            <p><span style="font-size: 18pt;"><a href="mailto:stone@granite.com.ua">stone@granite.com.ua</a></span></p>
                                            <p><span style="font-size: 18pt;"><a href="mailto:italy@granite.com.ua">italy@granite.com.ua</a></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="rt-grid-6 rt-omega">
                            <div class="rt-block ">
                                <div class="module-surround">
                                    <div class="module-content">


                                        <div class="custom"  >
                                            <h2>Склад камня  в Киеве</h2>

                                            <iframe src="https://www.google.com/maps/d/embed?mid=1BFpbHU5eU_7xl4CjdkCwagkl1II9lV-s" width="550" height="300"></iframe>
                                            <br />
                                            <!-- Google Code for &#1050;&#1086;&#1085;&#1090;&#1072;&#1082;&#1090;&#1099; Conversion Page -->
                                            <script type="text/javascript">
                                                /* <![CDATA[ */
                                                var google_conversion_id = 963251865;
                                                var google_conversion_language = "en";
                                                var google_conversion_format = "3";
                                                var google_conversion_color = "ffffff";
                                                var google_conversion_label = "_oQoCP6esF4QmZ2oywM";
                                                var google_remarketing_only = false;
                                                /* ]]> */
                                            </script>
                                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                                            </script>
                                            <noscript>
                                                <div style="display:inline;">
                                                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/963251865/?label=_oQoCP6esF4QmZ2oywM&amp;guid=ON&amp;script=0"/>
                                                </div>
                                            </noscript>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="rt-container">

                    <div id="rt-main" class="mb8-sa4">
                        <div class="rt-container">
                            <div class="rt-grid-8 ">
                                <div class="rt-block">
                                    <div id="rt-mainbody">
                                        <div class="component-content">
                                            <div class="contact">
                                                <h2>
                                                    <span class="contact-name">Контакты</span>
                                                </h2>
                                                <div id="contact-slider" class="pane-sliders"><div style="display:none;"><div>	</div></div><div class="panel"><h3 class="pane-toggler title" id="basic-details"><a href="javascript:void(0);"><span>Контакт</span></a></h3><div class="pane-slider content">

                                                            <dl class="contact-address dl-horizontal" itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                                                <dt>
			<span class="jicons-icons">
				<img src="/media/contacts/images/con_address.png" alt="Адреса: " />			</span>
                                                                </dt>

                                                                <dd>
				<span class="contact-street" itemprop="streetAddress">
					Жмеринская 22Б					<br />
				</span>
                                                                </dd>

                                                                <dd>
				<span class="contact-suburb" itemprop="addressLocality">
					Киев					<br />
				</span>
                                                                </dd>
                                                                <dd>
			<span class="contact-country" itemprop="addressCountry">
				Украина				<br />
			</span>
                                                                </dd>


                                                                <dt>
		<span class="jicons-icons">
			<img src="/media/contacts/images/con_tel.png" alt="Телефон: " />		</span>
                                                                </dt>
                                                                <dd>
		<span class="contact-telephone" itemprop="telephone">
			+38 (068) 686-54-75		</span>
                                                                </dd>
                                                                <dt>
		<span class="jicons-icons">
			<img src="/media/contacts/images/con_fax.png" alt="Факс: " />		</span>
                                                                </dt>
                                                                <dd>
		<span class="contact-fax" itemprop="faxNumber">
		+38 (098) 988-18-88		</span>
                                                                </dd>
                                                            </dl>

                                                            <p></p>

                                                        </div></div><div class="panel"><h3 class="pane-toggler title" id="display-form"><a href="javascript:void(0);"><span>Контактна форма</span></a></h3><div class="pane-slider content">						<div class="contact-form">
                                                                <form id="contact-form" action="/contacts/" method="post" class="form-validate form-horizontal well">
                                                                    <fieldset>
                                                                        <legend>Надіслати електронного листа</legend>
                                                                        <div class="control-group field-spacer">
                                                                            <div class="control-label">
                                                                                <span class="spacer"><span class="before"></span><span class="text"><label id="jform_spacer-lbl" class=""><strong class="red">*</strong> Обов'язкове поле</label></span><span class="after"></span></span>					</div>
                                                                            <div class="controls"> </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <div class="control-label">
                                                                                <label id="jform_contact_name-lbl" for="jform_contact_name" class="hasPopover required" title="Ім'я" data-content="Ваше ім'я">
                                                                                    Ім'я<span class="star">&#160;*</span></label>
                                                                            </div>
                                                                            <div class="controls"><input type="text" name="jform[contact_name]" id="jform_contact_name"  value="" class="required" size="30"       required aria-required="true"      />
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <div class="control-label">
                                                                                <label id="jform_contact_email-lbl" for="jform_contact_email" class="hasPopover required" title="Електронна адреса" data-content="Електронна пошта для контактів">
                                                                                    Електронна адреса<span class="star">&#160;*</span></label>
                                                                            </div>
                                                                            <div class="controls"><input type="email" name="jform[contact_email]" class="validate-email required" id="jform_contact_email" value=""
                                                                                                         size="30"    autocomplete="email"    required aria-required="true"  /></div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <div class="control-label">
                                                                                <label id="jform_contact_emailmsg-lbl" for="jform_contact_emailmsg" class="hasPopover required" title="Тема" data-content="Введіть тему вашого повідомлення тут.">
                                                                                    Тема<span class="star">&#160;*</span></label>
                                                                            </div>
                                                                            <div class="controls"><input type="text" name="jform[contact_subject]" id="jform_contact_emailmsg"  value="" class="required" size="60"       required aria-required="true"      />
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <div class="control-label">
                                                                                <label id="jform_contact_message-lbl" for="jform_contact_message" class="hasPopover required" title="Повідомлення" data-content="Введіть ваше повідомлення тут.">
                                                                                    Повідомлення<span class="star">&#160;*</span></label>
                                                                            </div>
                                                                            <div class="controls"><textarea name="jform[contact_message]" id="jform_contact_message"  cols="50"  rows="10" class="required"      required aria-required="true"     ></textarea></div>
                                                                        </div>
                                                                        <div class="control-group">
                                                                            <div class="control-label">
                                                                                <label id="jform_contact_email_copy-lbl" for="jform_contact_email_copy" class="hasPopover" title="Надіслати копію собі" data-content="Надіслати копію повідомлення на вказаною Вами адресою.">
                                                                                    Надіслати копію собі</label>
                                                                                <span class="optional">(необов'язково)</span>
                                                                            </div>
                                                                            <div class="controls"><input type="checkbox" name="jform[contact_email_copy]" id="jform_contact_email_copy" value="1" /></div>
                                                                        </div>
                                                                    </fieldset>
                                                                    <fieldset>
                                                                        <div class="control-group">
                                                                            <div class="control-label">
                                                                                <label id="jform_captcha-lbl" for="jform_captcha" class="hasPopover required" title="CAPTCHA" data-content="Введіть у текстове поле те, що Ви бачите на зображені.">
                                                                                    CAPTCHA<span class="star">&#160;*</span></label>
                                                                            </div>
                                                                            <div class="controls"><div id="jform_captcha" class="g-recaptcha  required" data-sitekey="6LdLhhYUAAAAACcuVUVtfhfkUm9rDadhPkIxpE72" data-theme="light" data-size="normal"></div></div>
                                                                        </div>
                                                                    </fieldset>
                                                                    <div class="control-group">
                                                                        <div class="controls">
                                                                            <button class="btn btn-primary validate" type="submit">Надіслати електронного листа</button>
                                                                            <input type="hidden" name="option" value="com_contact" />
                                                                            <input type="hidden" name="task" value="contact.submit" />
                                                                            <input type="hidden" name="return" value="" />
                                                                            <input type="hidden" name="id" value="1:contact" />
                                                                            <input type="hidden" name="8b9508a357dad870b53944465614e7b2" value="1" />			</div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div></div><div class="panel"><h3 class="pane-toggler title" id="display-misc"><a href="javascript:void(0);"><span>Інша інформація</span></a></h3><div class="pane-slider content">						<div class="contact-miscinfo">
                                                                <div class="jicons-icons">
                                                                    <img src="/media/contacts/images/con_info.png" alt="Інша інформація: " />					</div>
                                                                <div class="contact-misc">
                                                                    <p><span style="font-size: 18pt;">Наши телефоны Киев:</span></p>
                                                                    <p><span style="font-size: 18pt;">+38 (068) 686-54-75</span></p>
                                                                    <!--<p><span style="font-size: 18pt;">+38 (063) 634-78-39</span></p>
                                                                    <p><span style="font-size: 18pt;">+38 (067) 761-30-17</span></p>-->
                                                                    <p><span style="font-size: 18pt;">Наш телефон Львов:</span></p>
                                                                    <p><span style="font-size: 18pt;">+38 (098) 988-18-88</span></p>
                                                                </div>
                                                            </div>
                                                        </div></div></div></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rt-grid-4 ">
                                <div id="rt-sidebar-a">
                                    <div class="rt-block ">
                                        <div class="module-surround">
                                            <div class="module-content">
                                                <div class="custom"  >
                                                    <p><span style="font-size: 18pt;">OOO "Галлиардо"</span></p>
                                                    <p><strong><em><span style="font-size: 12pt;">Офіс в Італії</span></em></strong></p>
                                                    <p><span style="font-size: 12pt;">Verona, via Aquleia, 20</span></p>
                                                    <p><span style="font-size: 12pt;"><a href="mailto:italy@granite.com.ua">italy@granite.com.ua</a></span></p>
                                                    <p><span style="font-size: 12pt;">0039 393 4681852</span></p>
                                                    <p><strong><em><span style="font-size: 12pt;">Офіс в Україні</span></em></strong></p>
                                                    <p><span style="font-size: 12pt;">вул. Печеніжська, 8</span><br /><span style="font-size: 12pt;">Київ,&nbsp;</span><span style="font-size: 12pt;">Україна</span></p>
                                                    <p><strong><em><span style="font-size: 12pt;">Склад в Україні</span></em></strong></p>
                                                    <p><span style="font-size: 12pt;">вул.Жмеринська, 22Б</span><br /><span style="font-size: 12pt;">Київ,&nbsp;</span><span style="font-size: 12pt;">Україна</span></p>
                                                    <p><span style="font-size: 12pt;"><a href="mailto:stone@granite.com.ua">stone@granite.com.ua</a></span></p>
                                                    <!--<p><br /><span style="font-size: 12pt;">+38 (063) 634-78-39</span></p>
                                                    <p><span style="font-size: 12pt;">+38 (067) 761-30-17</span></p>-->
                                                    <p><span style="font-size: 12pt;">+38 (068) 686-54-75</span></p>
                                                    <p><span style="font-size: 12pt;">Skype <a href="skype:granite5">granite</a></span></p>
                                                    <p><span style="font-size: 12pt;">Пн.- Пт.&nbsp;&nbsp; 9 - 18</span></p>
                                                    <p>&nbsp;</p>
                                                    <p><strong><em><span style="font-size: 12pt;">Склад у Львові</span></em></strong></p>
                                                    <p><span style="font-size: 12pt;">вул. Зелена 149</span><br /><span style="font-size: 12pt;">Львів,&nbsp;</span><span style="font-size: 12pt;">Украина</span></p>
                                                    <p><a href="mailto:Lviv@granite.com.ua"><span style="font-size: 12pt;">Lviv@granite.com.ua</span></a></p>
                                                    <p><br /><span style="font-size: 12pt;">+38 (098) 988-18-88</span></p>
                                                    <p><span style="font-size: 12pt;">Пн.- Пт.&nbsp;&nbsp; 9 - 18</span></p></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>
                </div>


        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>