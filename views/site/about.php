<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 padding-right">
                <div class="features_items">
                    <h2 class="title text-center"></h2>
                    <!--<?php foreach ($categoryProducts as $portfolio): ?>-->
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src="<?php echo Portfolio::getImage($portfolio['id']); ?>" alt="" />
                                        <p>
                                            <a href="/product/<?php echo $product['id']; ?>">
                                                <?php echo $portfolio['name']; ?>
                                            </a>
                                        </p>
                                        <!--<a href="/cart/<?php echo $product['id']; ?>"
                                           class="btn btn-default add-to-cart" data-id="<?php echo $product['id']; ?>">
                                            <i href="/cart/" class="fa fa-shopping-cart"></i>В корзину</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
                <?php echo $pagination->get(); ?>

            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>