<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
        <div id="rt-fullscreen">

            <div class="rt-grid-12 rt-alpha rt-omega">
                <div class="rt-block ">
                    <div class="module-surround">
                        <div class="module-content">


                            <!-- START REVOLUTION SLIDER ver. 2.1.8 -->

                            <div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#e9e9e9;padding:0px;margin-top:0px;margin-bottom:0px;max-height:500px;direction:ltr;">
                                <div id="rev_slider_3_1" class="rev_slider fullwidthabanner" style="display:none;max-height:500px;height:500px;">
                                    <ul>

                                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >

                                            <img src="/upload/images/slider-1-20180627.jpg" alt="slider-1-20180627" />


                                            <!--<div class="tp-caption fade"
                                                 data-x="615"
                                                 data-y="355"
                                                 data-speed="300"
                                                 data-start="500"
                                                 data-easing="easeOutExpo"  ><img src="/images/logo.png" alt="Image 1"></div>-->

                                        </li>

                                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >

                                            <img src="/upload/images/slider-2-20180627.jpg" alt="slider-2-20180627" />


                                            <!--<div class="tp-caption fade"
                                                 data-x="10"
                                                 data-y="350"
                                                 data-speed="300"
                                                 data-start="500"
                                                 data-easing="easeOutExpo"  ><img src="/images/logo.png" alt="Image 1"></div>-->

                                        </li>

                                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >

                                            <img src="/upload/images/slider-3-20180627.jpg" alt="slider-3-20180627" />

                                        </li>

                                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >

                                            <img src="/upload/images/slider-4-20180627.jpg" alt="slider-4-20180627" />


                                            <!--<div class="tp-caption fade"
                                                 data-x="15"
                                                 data-y="355"
                                                 data-speed="300"
                                                 data-start="500"
                                                 data-easing="easeOutExpo"  ><img src="/images/logo.png" alt="Image 1"></div>-->

                                        </li>

                                        <li data-transition="random" data-slotamount="7" data-masterspeed="300" >

                                            <img src="/upload/images/slider-5-20180627.jpg" alt="slider-5-20180627" />


                                            <!--<div class="tp-caption fade"
                                                 data-x="15"
                                                 data-y="15"
                                                 data-speed="300"
                                                 data-start="500"
                                                 data-easing="easeOutExpo"  ><img src="/images/logo.png" alt="Image 1"></div>-->

                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <script type="text/javascript">

                                var tpj=jQuery;


                                var revapi3;

                                tpj(document).ready(function() {

                                    if (tpj.fn.cssOriginal != undefined)
                                        tpj.fn.css = tpj.fn.cssOriginal;

                                    if(tpj('#rev_slider_3_1').revolution == undefined)
                                        revslider_showDoubleJqueryError('#rev_slider_3_1',"joomla");
                                    else
                                        revapi3 = tpj('#rev_slider_3_1').show().revolution(
                                            {
                                                delay:10000,
                                                startwidth:1030,
                                                startheight:500,
                                                hideThumbs:200,

                                                thumbWidth:100,
                                                thumbHeight:50,
                                                thumbAmount:5,

                                                navigationType:"none",
                                                navigationArrows:"verticalcentered",
                                                navigationStyle:"round",

                                                touchenabled:"on",
                                                onHoverStop:"on",

                                                shadow:0,
                                                fullWidth:"on",

                                                navigationHAlign:"center",
                                                navigationVAlign:"bottom",
                                                navigationHOffset:0,
                                                navigationVOffset:20,

                                                stopLoop:"off",
                                                stopAfterLoops:-1,
                                                stopAtSlide:-1,

                                                shuffle:"off",

                                                hideSliderAtLimit:0,
                                                hideCaptionAtLimit:0,
                                                hideAllCaptionAtLilmit:0					});

                                });	//ready

                            </script>

                            <!-- END REVOLUTION SLIDER -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="clear"></div>

        </div>
        <div id="rt-showcase">
            <div class="rt-showcase-pattern">
                <div class="rt-container">
                    <div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block ">
                            <div class="module-surround">
                                <div class="module-content">
                                    <div class="custom"  >
                                        <h1>Вироби з оніксу, мармуру, граніту. Продаж слябів</h1></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div id="galliardo-home">

                <div id="rt-mainbody-surround">
                    <div id="rt-maintop">
                        <div class="rt-container">
                            <div class="rt-grid-12 rt-alpha rt-omega">
                                <div class="rt-block ">
                                    <div class="module-surround">
                                        <div class="module-content">
                                            <div class="custom"  >
                                                <p style="text-align: justify;">&nbsp;&nbsp; Компанія <strong>Granit</strong> була заснована в невеликому місті на півночі Італії в 2009 році. Це місто являється законодавцем моди у світі каменеобробки та у всьому, що пов’язано з натуральним каменем. Тут проходять наймасштабніші у світі виставки натурального каменю, а також обладнання, що пов’язане з його обробкою. Це місто відоме всьому світу , як місце, де відбулася найтрагічніша історія кохання всіх часів та народів , яку описав у своєму шедеврі «Ромео та Джульєтта» Вільям Шекспір! Напевно Ви вже здогадалися, про яке місто йде мова?! Так, вірно – це Верона!<br />&nbsp;&nbsp; Всі найвідоміші каменеобробні підприємства знаходяться саме тут. Уже не одне століття поспіль вони дивують увесь світ новими кольорами та структурами каменю, що завозиться з усіх куточків світу і обробляється на новітньому обладнанні, використовуючи при цьому досягнення останніх технологій для його виробництва.<br />&nbsp;&nbsp; Компанія <strong>Granit</strong> має своє постійне представництво в місті Верона, саме тому може запропонувати Вам не лише найкращий камінь на складі в Києві, але і можливість Вам самостійно приїхати до Італії, де Вас зустрінуть і проведуть на найкращі заводи-виробники каменю. На цих складах будь-яка Ваша забаганка буде здаватися закономірністю. Більш детально з цією пропозицією Ви можете ознайомитися в розділі «За каменем до Італії».<br />&nbsp;&nbsp;&nbsp; Починаючи з 2011 року Компанія <strong>Granit</strong> відкрила своє постійне представництво і в Україні.<br />&nbsp;&nbsp; Звертаючись до компанії <strong>Granit</strong>, ви обираєте надійного партнера, який відбере і привезе для Вас в найкоротші терміни камінь з будь-якої країни світу. Більш ніж 10-річний досвід роботи на ринку каменю дозволяє нам впевнено виконувати проекти будь-якого рівня складності, з урахуванням всіх особливостей та побажань замовника.<br />&nbsp;&nbsp; Спеціалісти компанії <strong>Granit</strong> допоможуть Вам спроектувати дизайнерське рішення для будь-якого типу приміщення, починаючи з вітальні й закінчуючи ванною кімнатою.<br />&nbsp;&nbsp; Синонім слова <strong>Granit</strong>, у перекладі з італійської мови – сильний, доблесний, чесний. Саме таким є колектив нашої компанії. Завітайте до нас і переконайтеся в цьому!</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rt-block title4">
                                    <div class="module-surround">
                                        <div class="module-title">
                                            <h2 class="title"><span>За каменем до Італії!</span></h2>
                                            <div class="title-line">
                                                <span></span>
                                            </div>
                                        </div>
                                        <div class="module-content">


                                            <div class="customtitle4"  style="background-image: url(/upload/images/backgraund/home_itali.png)" >
                                                <div style="display: block; width: 100%; height: 280px;">
                                                    <div style="padding: 15px; float: left;">
                                                        <p><img style="vertical-align: middle;" src="/upload/images/za-kamnem.jpg" alt="itali" width="250" height="250" /></p>
                                                    </div>
                                                    <div style="padding: 15px 15px 40px 330px;">
                                                        <h3>Нова унікальна послуга</h3>
                                                        <strong>&nbsp;&nbsp; За каменем до Італії</strong> – це нова послуга на ринку декоративного каменю України, яку пропонує компанія Granit. Маючи своє постійне представництво у місті Верона, ми пропонуємо Вам здійснити свою мрію – поїздку на кар’єри, де і добувають той самий заповітний камінь, що Ви мріяли мати у себе вдома. Відвідати ексклюзивні підприємства світу по обробці і видобутку натурального каменю. І, нарешті, обрати саме той колір і структуру каменю, яку Ви собі уявляли.<br />&nbsp;&nbsp;&nbsp; Ми підтримуємо великий асортимент каменю на складі в Києві, щоб догодити найвибагливішому клієнту. Але повірте нам, це всього лиш крапля у морі, порівнюючи з тим, що Ви можете побачити на складах наших партерів в Італії. </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="rt-container">
                    </div>
                </div>


        </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>